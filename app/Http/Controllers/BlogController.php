<?php

namespace App\Http\Controllers;

use App\Blog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class BlogController extends Controller
{
    public function createBlog(){
        return view ('blog.create');
    }

    public function storeBlog(Request $request){
        $data = $request->all();

        $validatedData = $request->validate([
            'title' => 'required',
            'content' => 'required',
            'image' => 'required'
        ]);

        $blog = new Blog();
        $blog->title = $data['title'];
        $blog->content = $data['content'];
        $random = Str::random(10);
        if($request->hasFile('image')){
            $image_tmp = $request->file('image');
            if($image_tmp->isValid()){
                $extension = $image_tmp->getClientOriginalExtension();
                $filename = $random.'.'.$extension;
                $image_path = 'uploads/'.$filename;
                Image::make($image_tmp)->save($image_path);
                $blog->image = $filename;
            }
        }
        $blog->save();
        Session::flash('success_message', 'Post Has Been Added Successfully');
        return redirect()->route('viewBlog');
    }

    public function viewBlog(){
        $blogs = Blog::latest()->get();
        return view ('blog.index', compact('blogs'));
    }
}
